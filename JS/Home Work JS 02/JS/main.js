
let sizeShape = parseInt(prompt("Input odd number"));


if (sizeShape % 2 === 0) {
  document.write("number is wrong!");
} else {

  document.write("<h2>Rectangle</h2>");
  for (let i = 0; i < sizeShape; i++) {
    for (let j = 0; j < sizeShape * 2; j++) {
      document.write("*");
    }
    document.write("<br>");
  }

  document.write("<h2>Right triangle</h2>");
  for (let i = 0; i < sizeShape; i++) {
    for (let j = 0; j < sizeShape; j++) {
      if (j > i) {
        document.write("&nbsp;");
      } else {
        document.write("*");
      }
    }
    document.write("<br>");
  }

  document.write("<h2>Romb</h2>");
  let flagOne = 0;
  for (let i = 0; i < sizeShape; i++) {
    if (i > parseInt(sizeShape / 2)) {
      flagOne++;
    }
    for (let j = 0; j < sizeShape; j++) {
      if (i > parseInt(sizeShape / 2)) {

        if ((j >= flagOne) && (j <= (sizeShape - 1) - flagOne)) {
          document.write("*");
        } else {
          document.write("&nbsp;");
        }
      } else {
        if ((j < parseInt(sizeShape / 2 - i)) || (j > parseInt(sizeShape / 2 + i))) {
          document.write("&nbsp;");
        } else {
          document.write("*");
        }
      }
    }
    document.write("<br>");
  }

  document.write("<h2>Christmas tree</h2>");
  for (let i = 0; i < sizeShape; i++) {
    if (i == 0) {
      for (let j = 0; j < parseInt(sizeShape / 2 + 1); j++) {
        for (let k = 0; k < sizeShape; k++) {
          if ((k < parseInt(sizeShape / 2 - j)) || (k > parseInt(sizeShape / 2 + j))) {
            document.write("&nbsp;");
          } else {
            document.write("*");
          }
        }
        document.write("<br>");
      }
    } else if (i == sizeShape - 1) {
      for (let j = 0; j < parseInt(sizeShape / 2); j++) {
        for (let k = 0; k < sizeShape; k++) {
          if (k == parseInt(sizeShape / 2)) {
            document.write("*");
          } else {
            document.write("&nbsp;");
          }
        }
        document.write("<br>");
      }
    } else {
      for (let j = 1; j < parseInt(sizeShape / 2 + 1); j++) {
        for (let k = 0; k < sizeShape; k++) {
          if ((k < parseInt(sizeShape / 2 - j)) || (k > parseInt(sizeShape / 2 + j))) {
            document.write("&nbsp;");
          } else {
            document.write("*");
          }
        }
        document.write("<br>");
      }
    }
  }

}


