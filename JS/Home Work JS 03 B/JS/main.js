document.write("<ol><li>Даны два массива: ['a', 'b', 'c'] и [1, 2, 3]. Объедините их вместе.");
let arrayOne = ['a', 'b', 'c'];
let arrayTwo = [1, 2, 3];
let resultArray = arrayOne.concat(arrayTwo);
document.write("<p> Результат: " + resultArray.join(", ") + "<hr>");

document.write("<li>Дан массив ['a', 'b', 'c']. Добавьте ему в конец элементы 1, 2, 3.");
arrayTwo = arrayTwo.join(", ");
arrayOne.push(arrayTwo);
document.write("<p> Результат: " + arrayOne.join(", ") + "<hr>");

document.write("<li>Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1].");
arrayTwo = [1, 2, 3];
arrayTwo.reverse();
document.write("<p> Результат: " + arrayTwo.join(", ") + "<hr>");

document.write("<li>Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6.");
arrayTwo = [1, 2, 3];
arrayOne = [4, 5, 6].join(", ");
arrayTwo.push(arrayOne);
document.write("<p> Результат: " + arrayTwo.join(", ") + "<hr>");

document.write("<li>Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6.");
arrayTwo = [1, 2, 3];
arrayTwo.unshift(arrayOne);
document.write("<p> Результат: " + arrayTwo.join(", ") + "<hr>");

document.write("<li>Дан массив ['js', 'css', 'jq']. Выведите на экран первый элемент.");
arrayOne = ['js', 'css', 'jq'];
document.write("<p> Результат: " + arrayOne[0] + "<hr>");

document.write("<li>Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый массив элементы [1, 2, 3].");
arrayOne = [1, 2, 3, 4, 5];
arrayTwo = arrayOne.slice(0, 3);
document.write("<p> Результат: " + arrayTwo.join(", ") + "<hr>");

document.write("<li>Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5].");
arrayOne.splice(1, 2);
document.write("<p> Результат: " + arrayOne.join(", ") + "<hr>");

document.write("<li>Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 2, 10, 3, 4, 5].");
arrayOne = [1, 2, 3, 4, 5];
arrayOne.splice(2, 0, 10);
document.write("<p> Результат: " + arrayOne.join(", ") + "<hr>");

document.write("<li>Дан массив [3, 4, 1, 2, 7]. Отсортируйте его.");
arrayOne = [3, 4, 1, 2, 7];
arrayOne.sort();
document.write("<p> Результат: " + arrayOne.join(", ") + "<hr>");

document.write("<li>Дан массив с элементами 'Привет, ', 'мир' и '!'. Необходимо вывести на экран фразу 'Привет, мир!'.");
arrayOne = ["Привет, ", "мир", "!"];
document.write("<p> Результат: " + arrayOne.join("") + "<hr>");

document.write("<li>Дан массив ['Привет, ', 'мир', '!']. Необходимо записать в нулевой элемент этого массива слово 'Пока, ' (то есть вместо слова 'Привет, ' будет 'Пока, ').");
arrayOne[0] = "Пока, ";
document.write("<p> Результат: " + arrayOne.join("") + "<hr>");

document.write("<li>Создайте массив arr с элементами 1, 2, 3, 4, 5 двумя различными способами.");
arrayOne = [1, 2, 3, 4, 5];
arrayTwo = new Array(1, 2, 3, 4, 5);
document.write("<p> Результат: Массив 1 = " + arrayOne.join(", ") + ". Массив 2 = " + arrayTwo.join(", ") + "<hr>");

document.write("<li>Создайте массив arr = ['a', 'b', 'c', 'd'] и с его помощью выведите на экран строку 'a+b, c+d'.");
arrayOne = ["a", "b", "c", "d"];
document.write("<p> Результат: " + arrayOne[0] + arrayOne[1] + ", " + arrayOne[2] + arrayOne[3] + "<hr>");

document.write("<li>Запросите у пользователя количество элементов массива. Исходя из данных которые ввел пользователь создайте массив на то количество элементов которое передал пользователь. в кажlом индексе массива храните чило которе будет показывать номер элемента массива.");
let sizeArray = prompt("Input size Array:");
arrayOne = new Array(parseInt(sizeArray));
for (let i = 0; i < arrayOne.length; i++) {
  arrayOne[i] = i;
}
document.write("<p> Результат: " + arrayOne.join(", ") + "<hr>");

document.write("<li>Сделайте так, чтобы из массива который вы создали выше вывелись все нечетные числа в параграфе, а четные в спане с красным фоном.");
document.write("<p> Результат: </p>");
arrayOne.forEach(function(item){
  if(item % 2 !== 0) {
    document.write("<p class='green'>Число " + item + " в параграфі</p>");
  } else {
    document.write("<span class = 'red'>Число " + item + " в спані</span>");
  }
});
document.write("<hr>");

document.write("<li>Напишите код, который преобразовывает и объединяет все элементы массива в одно строковое значение. Элементы массива будут разделены запятой.");
var vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
let str1 = "<p>" + vegetables.join(", ");
document.write(str1); // "Капуста, Репа, Редиска, Морковка"

