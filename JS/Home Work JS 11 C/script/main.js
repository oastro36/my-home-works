/* 
Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
*/

window.addEventListener("DOMContentLoaded", () => {
  const bodyWidth = document.body.clientWidth;
  const bodyHeight = document.body.clientHeight;

  const mouse = (e) => {
    const randomCoordX = Math.floor(Math.random() * (bodyWidth - 100));
    const randomCoordY = Math.floor(Math.random() * (bodyHeight - 100));
    e.target.style.left = randomCoordX + "px";
    e.target.style.top = randomCoordY + "px";

  }

  const redDiv = document.querySelector("div");

  redDiv.addEventListener("mouseover", mouse);
});
