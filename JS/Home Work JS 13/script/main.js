
window.addEventListener("DOMContentLoaded", () => {

  function createCard(inf) {
    let card = document.createElement("div");
    card.classList.add("card");
    
    card.innerText = inf;
    const container = document.querySelector(".container");
    
    container.append(card);
  }

  try {
    let portion = "https://swapi.dev/api/people/";
    if(portion === null) {
      document.getElementById("buttonGet").setAttribute(disable, disable);
    }
    const xhr = new XMLHttpRequest();
    document.getElementById("buttonGet").onclick = function () {

      xhr.open("GET", portion);
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 300)) {

          const data = JSON.parse(xhr.responseText);
          
              data.results.forEach(element => {
                const{name, height, mass, hair_color, skin_color, eye_color, birth_year, gender} = element
                const info = `name: ${name}\r\n height: ${height}\r\n mass: ${mass}\r\n hair_color: ${hair_color}\r\n skin_color: ${skin_color}\r\n eye_color: ${eye_color}\r\n birth_year: ${birth_year}\r\n gender: ${gender}`;
                createCard(info);

              });

                portion = data.next;
        }
      }
      xhr.send();

    }

  } catch (error) {
    throw new Error(error)
    console.error(error)
  };
});