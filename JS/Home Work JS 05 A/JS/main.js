const myDocument = {
  header1: "My Header",
  body1: "My Body",
  footer1: "My Footer",
  date1: "Date: 24.08.2022",
 
  application: {
    myHeader: {
      tag: ""
    },
    myBody: {
      tag: ""
    },
    myFooter: {
      tag: ""
    },
    myDate: {
      tag: ""
    }
  },

  fillingObjects: function (obj1, obj2, obj3) {
    this.application.myHeader.tag = obj1;
    this.application.myBody.tag = obj2;
    this.application.myFooter.tag = obj3;
  },

  showDocument: function() {
    for (let element in myDocument.application) {
      document.getElementById("container").innerHTML += `${myDocument.application[element].tag}`;
    }
  }
}

const headerObj = `<div class='my-header'>${myDocument.header1}</div>`;
const bodyObj = `<div class='my-body'>${myDocument.body1}<p>${myDocument.date1}</p></div>`;
const footerObj = `<div class='my-footer'>${myDocument.footer1}</div>`;

myDocument.fillingObjects(headerObj, bodyObj, footerObj);

myDocument.showDocument();
