/* 
Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
При наведенні мишки виведіть текст координати, де знаходиться курсор мишки
*/

window.addEventListener("DOMContentLoaded", () => {
  const mainDiv = document.querySelector("div:nth-child(1)");
  const coordinatsDiv = document.querySelector("div:nth-child(2)");

  mainDiv.onmousemove = function(e) {
    coordinatsDiv.innerHTML = `X:${e.offsetX}, Y:${e.offsetY}`;
  }

  mainDiv.onmouseout = function() {
    coordinatsDiv.innerHTML = `X:0, Y:0`;
  }
})
