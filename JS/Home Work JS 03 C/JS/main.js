document.getElementById("container").innerHTML += `Дан многомерный массив arr:
<pre>var arr = {
  'ru':['голубой', 'красный', 'зеленый'],
  'en':['blue', 'red', 'green'],
};</pre>
<p>Выведите с его помощью слово 'голубой' 'blue' .</p> Результат: `;

const arr = {
  'ru':['голубой', 'красный', 'зеленый'],
  'en':['blue', 'red', 'green'],
};

function textFromObject(array) {
 document.getElementById("container").innerHTML += `${array[0]} `; 
}

textFromObject(arr.ru);
textFromObject(arr.en);