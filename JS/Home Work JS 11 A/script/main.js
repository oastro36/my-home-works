/* Створи список, що складається з 4 аркушів. 
Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний
 */

window.addEventListener("DOMContentLoaded", () => {
  const [...lists] = document.getElementsByTagName("li");
  const myDiv = document.querySelector("div");

  for (let i = 0; i < lists.length; i++) {
   
    if (i === 0) {
      lists[i].style.backgroundColor = "#37b4f4";
    }
    if (i === 1) {
      myDiv.innerText = `Звернулися до другого елементу і вивели його вміст: ${lists[i].outerText}`;
    }
    if(i === 2) {
      lists[i].style.backgroundColor = "#cf37f4";
    }
    if (i === lists.length - 1) {
      continue;
    }
  }
});