let minusFlag = 0;
let dotFlag = 0;
let numFlag = 0;
let classFlag = 0;
let arrayCalcString = "";
let number = 1;
let result = 0;
let memory = "";

const display = document.querySelector(".display > input");
const resulBtn = document.getElementById("=");
const cancelBtn = document.getElementById("C");
const addToMemoryBtn = document.getElementById("m+");
const clearMemoryBtn = document.getElementById("m-");
const getMemoryBtn = document.getElementById("mrc");
const m = document.querySelector(".memory");

window.addEventListener("DOMContentLoaded", () => {

  const keys = document.querySelector(".keys");

  keys.addEventListener("click", (e) => {
    myCalc(e.target.value);
  })
});

function myCalc(itm) {

  if (minusFlag === 0 && itm === "-") {
    display.value += itm;
    arrayCalcString += itm;
    minusFlag = 1;
  }

  if (dotFlag === 0 && itm === ".") {
    display.value += itm;
    arrayCalcString += itm;
    dotFlag = 1;
  }

  const pattern = /[0-9]/;
  if (pattern.test(itm)) {
    display.value += itm;
    arrayCalcString += itm;
    numFlag = 1;
  }

  if (numFlag === 1) {
    if (classFlag === 0) {
      if (itm === "/" || itm === "*" || itm === "-" || itm === "+") {
        calculator.number1 = parseFloat(arrayCalcString);
        calculator.sign = itm;
        arrayCalcString = "";
        display.value = "";
        classFlag = 1;
        minusFlag = 0;
        dotFlag = 0;
      }
    } else {
      calculator.number2 = parseFloat(arrayCalcString);
      resulBtn.removeAttribute("disabled");
      
      switch (calculator.sign) {
        case "+": result = add(calculator.number1, calculator.number2);
          break;
        case "-": result = sub(calculator.number1, calculator.number2);
          break;
        case "*": result = mul(calculator.number1, calculator.number2);
          break;
        case "/": result = div(calculator.number1, calculator.number2);
          break;
      }
    }
  }
}

resulBtn.addEventListener("click", function (e) {
  display.value = result;
});

cancelBtn.addEventListener("click", function (e) {
  display.value = "";
  arrayCalcString = "";
  calculator.number1 = "";
  calculator.sign = "";
  calculator.number2 = "";
});

addToMemoryBtn.addEventListener("click", function (e) {
  memory = display.value;
  m.style.display = "block";
});

clearMemoryBtn.addEventListener("click", function (e) {
  memory = "";
  m.style.display = "none";
})

getMemoryBtn.addEventListener("click", function (e) {
  display.value = memory;
})

const calculator = {
  number1: "",
  sign: "",
  number2: ""
}

function add(x1, x2) {
  return x1 + x2;
}

function sub(x1, x2) {
  return x1 - x2;
}

function mul(x1, x2) {
  return x1 * x2;
}

function div(x1, x2) {
  if (x2 !== 0) {
    return x1 / x2;
  } else {
    return 0;
  }
}
