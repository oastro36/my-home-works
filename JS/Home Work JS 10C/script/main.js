
window.addEventListener("DOMContentLoaded", (e) => {

  function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return 'rgba(' + r + ',' + g + ',' + b + ',1)';
  }

  const container = document.createElement("div");

  function drawingBubbles(r) {
    container.classList.add("container-bubbles");
    document.body.append(container);

    for (let i = 0; i < 100; i++) {
      const bubble = document.createElement("div");
      bubble.classList.add("one-bubble");
      bubble.style.width = `${r}px`;
      bubble.style.height = `${r}px`;
      bubble.style.backgroundColor = getRandomColor();
      container.append(bubble);
    }
  }

  container.addEventListener("click", function (e) {
    let b = e.target;
    b.remove();
    e.stopPropagation();
  }, false);

  const button1 = document.createElement("button");
  button1.innerText = "Draw bubbles";

  document.body.prepend(button1);

  button1.addEventListener("click", (e) => {
    for (let flag = true; flag === true;) {
      const radius = parseInt(prompt("Input radius", "from 50 to 100"));
      if (radius >= 50 && radius <= 100) {
        flag = false;
        drawingBubbles(radius);
      } else {
        alert("Radius incorrect!");
      }
    }
  });
});