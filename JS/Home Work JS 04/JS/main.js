const initialArray = [5,7,8,9,6,4,1,5,6,7,4,5,6,5,7,8,9,5,4,7];

const mulIndex = (i) => {
return i * i;
}

function map(fn, array) {
  const newArray = [];
  for(let i = 0; i < array.length; i++) {
    newArray[i] = fn(i);
  }
  return newArray;
}

const arrayAfterMod = map(mulIndex, initialArray);

document.getElementById("resultat").innerHTML = `<p class="description">Array after modification:</p> 
<p>${arrayAfterMod.join(", ")}</p>
<p class="description">Перезаписана функція за допомогою тернарного оператора</p>
<pre>
function checkAge(age) {
  return age > 18 ? true : confirm('Батьки дозволили?');
}
</pre>`;

