const startBotton = document.getElementById("start");
const stopBotton = document.getElementById("stop");
const resetBotton = document.getElementById("reset");
const stopwatch = document.querySelector(".container-stopwatch");
const secodns = document.getElementById("sec");
const minutes = document.getElementById("min");
const hour = document.getElementById("hrs");
let runner = null;

window.onload = () => {
  startBotton.onclick = () => {

    runner = setInterval(go, 1000);
    startBotton.setAttribute("disabled", "disabled");
    const [...classes] = stopwatch.classList;
    classes.forEach((item) => {
      if (item !== "container-stopwatch") {
        stopwatch.classList.replace(item, "green");
      }
    });
  }

  stopBotton.onclick = () => {
    startBotton.removeAttribute("disabled", "disabled");
    clearInterval(runner);
    const [...classes] = stopwatch.classList;
    classes.forEach((item) => {
      if (item !== "container-stopwatch") {
        stopwatch.classList.replace(item, "red");
      }
    });
  }

  resetBotton.onclick = () => {
    startBotton.removeAttribute("disabled", "disabled");
    secodns.textContent = "00";
    minutes.textContent = "00";
    hour.textContent = "00";
    const [...classes] = stopwatch.classList;
    classes.forEach((item) => {
      if (item !== "container-stopwatch") {
        stopwatch.classList.replace(item, "silver");
      }
    });
  }

  const go = () => {
    const watchTime = parseInt(secodns.textContent) + 1;
    if (watchTime < 60) {
      if (watchTime < 10) {
        secodns.textContent = "0" + watchTime;
      } else {
        secodns.textContent = parseInt(secodns.textContent) + 1;
      }
    } else {
      secodns.textContent = "00";
      const watchTimeMinutes = parseInt(minutes.textContent) + 1;
      if (watchTimeMinutes < 60) {
        if (watchTimeMinutes < 10) {
          minutes.textContent = "0" + watchTimeMinutes;
        } else {
          minutes.textContent = parseInt(minutes.textContent) + 1;
        }
      } else {
        minutes.textContent = "00";
        const watchTimeHour = parseInt(hour.textContent) + 1;
        if (watchTimeHour < 60) {
          if (watchTimeHour < 10) {
            hour.textContent = "0" + watchTimeHour;
          } else {
            hour.textContent = watchTimeHour + 1;
          }
        } else {
          hour.textContent = "00";
        }
      }
    }
  }
}

