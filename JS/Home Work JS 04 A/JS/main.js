document.getElementById("container").innerHTML = `<p class='p-bold'>Задачка на роботу функції ggg:`;

const threeFunc = function() {
  return 3;
}

const fourFunc = function() {
  return 4;
}

function ggg(fOne, fTwo) {
  return fOne + fTwo;
}

let result = ggg(threeFunc(), fourFunc());

document.getElementById("container").innerHTML += `<pre>
const threeFunc = function() {
  return 3;
}

const fourFunc = function() {
  return 4;
}

function ggg(fOne, fTwo) {
  return fOne + fTwo;
}

let result = ggg(threeFunc(), fourFunc());
</pre>`;

document.getElementById("container").innerHTML += `Результат роботи функції ggg = ${result}<hr>`;
document.getElementById("container").innerHTML += `<p class='p-bold'>Задачка на роботу функції яка рахує і виводить кількість своїх викликів:`;

let count = 0;
function myFunction() {
  count++;
}

myFunction();
myFunction();
myFunction();
myFunction();

document.getElementById("container").innerHTML += `<pre>
let count = 0;
function myFunction() {
  count++;
}

myFunction();
myFunction();
myFunction();
myFunction();
</pre>`;

document.getElementById("container").innerHTML += `Функція myFunction() була викликана ${count} рази(ів)`;