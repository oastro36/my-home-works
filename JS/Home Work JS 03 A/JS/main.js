const stylesOfMusic = ["Jazz", "Blues"];

stylesOfMusic.push("Rock'n'roll");

document.write("<p> <span>Array after pasting at the end:</span> <br />" + stylesOfMusic.join(", "));

const classic = "Classic";

let centerElement = Math.floor(stylesOfMusic.length / 2);

if (Math.floor(stylesOfMusic.length % 2) !== 0) {
  stylesOfMusic[centerElement] = classic;
  document.write("<p> <span>Array after insert in the center: </span><br />" + stylesOfMusic.join(", "));

} else {
  stylesOfMusic.splice(centerElement, 0, classic);
  document.write("<p> <span>Array after insert in the center: </span><br />" + stylesOfMusic.join(", "));
}

const firstElement = stylesOfMusic.shift();
document.write("<p><span>First element: </span>" + firstElement + "<br />");
document.write("<p><span>Array after removing the first element:</span> <br />");
document.write(stylesOfMusic.join(", "));

stylesOfMusic.unshift(["Rap", " Reggae"]);
document.write("<p> <span>Array after insert two elements at the begin:</span> <br />");
document.write(stylesOfMusic.join(", "));

