
function createTag(tagName = "", classUserName = "", tagIdName = "", content = "") {
  if (tagName === null || tagName === "") {
    console.error("Назва тегу відсутня");
    return "";
  }

  const newTag = document.createElement(tagName);

  if (classUserName !== "" || classUserName !== null) {
    newTag.className = classUserName;
  }

  if (tagIdName !== "" || tagIdName !== null) {
    newTag.id = tagIdName;
  }

  if (content !== "" || content !== null) {
    newTag.textContent = content;
  }
  return newTag;
}

export default createTag