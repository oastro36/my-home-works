import createTag from "./createTag.js";

window.onload = () => {
  const head = document.querySelector("title");
  const metaDescr = document.createElement("meta");
  metaDescr.setAttribute("name", "description");
  metaDescr.setAttribute("content", "My site");
  head.before(metaDescr);

  const body = document.body;
  body.textContent = "Body";

  const topHeader = createTag("header", "top-header-one", null, "Header");
  body.append(topHeader);
  
  const topNav = createTag("nav", "nav-one", null, "Nav");
  topHeader.append(topNav);
 
  const main = createTag("main", "main-one", null, " ");
  body.append(main);

  const sectionLeft = createTag("section", "section-l", null, "section");
  const sectionRight = createTag("section", "section-r", null, "section");
  main.append(sectionLeft, sectionRight);

  const sectionHeader = createTag("header", "section-header", null, "Header");
  sectionLeft.append(sectionHeader);

  const article = createTag("article", "article", null, "Article");
  sectionLeft.append(article);
  const headerArticle = createTag("header", "header-article", null, "Header");
  const pFull = createTag("p", "full-paragraph", null, "p");
  const pHalf = createTag("p", "half-paragraph", null, "p");
  const aside = createTag("aside", "aside", null, "Aside");
  const footer = createTag("footer", "footer-section", null, "Footer");

  article.append(headerArticle, pFull, pHalf, aside, footer);

  const articleBottom = createTag("article", "article", null, "Article");
  sectionLeft.append(articleBottom);
  const bottomHeader = createTag("header", "header-article", null, "Header");
  const bottomFull = createTag("p", "full-paragraph", null, "p");
  const bottomFullTwo = createTag("p", "full-paragraph", null, "p");
  const bottomFooter = createTag("footer", "bottom-footer-blue", null, "Footer");

  articleBottom.append(bottomHeader, bottomFull, bottomFullTwo, bottomFooter);

  const bottomFooterSection = createTag("footer", "bottom-footer-section", null, "Footer");
  sectionLeft.append(bottomFooterSection);

  const headerRight = createTag("header", "section-header", null, "Header");
  const navRight = createTag("nav", "right-nav", null, "Nav");

  sectionRight.append(headerRight, navRight);

  const bodyFooter = createTag("footer", "body-footer", null, "Footer");

  body.append(bodyFooter);

  body.addEventListener("click", function(e){
    e.target.innerText = prompt("Input text");
  
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
  
    e.target.style.backgroundColor = 'rgba(' + r + ',' + g + ',' + b + ',0.8)';
    e.stopPropagation();
  },true);
  
  
}

