const typeOfCompare = prompt("Input kind of sorting('+' - up, '-' - down)");

function Human(name, lastName, age, sex) {
  this.name = name,
    this.lastName = lastName,
    this.age = age,
    this.sex = sex
}

Human.prototype.getPersonInfo = function (person) {
  document.getElementById("container").innerHTML += `<span>Person</span>: ${this.name} ${this.lastName}, ${this.age}, ${this.sex}<br>`;
}

const person1 = new Human("Ivan", "Ivanov", 35, true);
const person2 = new Human("Petr", "Petrov", 46, true);
const person3 = new Human("Sasha", "Sidorov", 14, true);
const person4 = new Human("Lena", "Ivanova", 25, false);
const person5 = new Human("Viktor", "Petrov", 39, true);

const humanArray = new Array(person1, person2, person3, person4, person5);

document.getElementById("container").innerHTML += `<p>Array of people befor sorting:</p>`;
function getPersonInfoFromArray(arr) {
  arr.forEach(function (item) {
    item.getPersonInfo();
  });
}

getPersonInfoFromArray(humanArray);

function directionSort(direction) {
  if (direction === '+') {
    function sortHuman(p1, p2) {
      return p1.age - p2.age;
    }
    document.getElementById("container").innerHTML += `<p>Array of people after sorting:</p>`;
    getPersonInfoFromArray(humanArray.sort(sortHuman));
  } else if (direction === '-') {
    function sortHuman(p1, p2) {
      return (p1.age - p2.age) * -1;
    }
    document.getElementById("container").innerHTML += `<p>Array of people after sorting:</p>`;
    getPersonInfoFromArray(humanArray.sort(sortHuman));
  } else {
    document.getElementById("container").innerHTML += `<p class='p-error'>Value is wrong!</p>`;
  }
}

directionSort(typeOfCompare);

getPersonInfoFromArray(humanArray.sort(sortHuman));



